https://gitlab.com/Juin/imie-python-20190128

# Cours Python IMIE 2019-01-28

## Lien de cours

* https://python.developpez.com/tutoriels/apprendre-programmation-python/les-bases/?page=ordinateur-et-programmation
* http://book.pythontips.com/en/latest/decorators.html

## Jours 1 - généralitées sur python

* Variable
* Portée
* Fonctions
* Classes
* Listes
* Dictionnaires

## Jours 2 - TP: bataille navale

Mise en pratique des notions:

* Classes
* Lambda
* map
* dico
* liste
* input
* for
* fonctions
* UML - (Use case, diagramme de classes, diagramme de séquence)

## Jours 3 - Connecteur BDD, suite TP: bataille navale

* argparse
     Utiliser argparse dans votre main.py, pour avoir l'aide.
     python main.py -h
         "Afficher l'aide"
     python maine.py -c chemin/du/fichier/config.json
         Lance le programme et charge le fichier passé en parametre

* sqlalchemy
   * Ajouter une option (avec argparse) pour afficher les High-Score
   * Les high-score seront stockés dans une table sqlite.
   * Il faudra simplement lire la table avec sqlAlchemy, puis afficher les
     lignes de cette table
   * Puis afficher les high_score de la base en utilisant l'ORM de sqlalchemy

* socket
   * Commencer la couche reseau pour le jeu
   * Doc socket: https://docs.python.org/3/library/socket.html#example
   * https://docs.python.org/3/howto/sockets.html#socket-howto
   * Ajouter une option mode_server
   * Ajouter une option mode_client
   * Définir le protocol

## Jours 4 - Python & WEB: Flask, Bottle, Django

* Flask
   * Afficher la table des scores (TP bataille navale)
     dans une page web avec Flask et Sqlalchemy
* Django
   * Faire un gestionnaire de commande.
      * Un model Customer
      * Un model Article
      * Un model Commande
      * Afficher une commande
      * Utiliser DjangoAdmin pour saisir les données.

## Jours 5 - Suite TP: Bataille Navaele,
   * Utilisation GIT
   * Implementation complete de la boucle de jeux
   * Implementation de la couche reseau
   * Implementation de la couche graphiqu

## Jours * - TP: Super Hero Rampage

## Jours * - PyGtk, PyQT

## Jours * - NumPy

## Jours * - Plotly

## Jours * - Graphen

## Jours * - Threading, mulitprocessing

## Jours * - PyGame

## Jours * - asyncio
