from models.grid import Grid 
from models.engine import Engine


if __name__ == "__main__":
    e = Engine()
    for p in e.players:
        print(p.name, 'grid: ')
        print(p.grid)
        print("-"*10)
    e.start()
