import json
from pprint import pprint

if __name__ == "__main__":
    with open('data.json', 'rt') as f:
        ma_list = json.load(f)
        
    print(type(ma_list))
    pprint(ma_list)
    pprint(dir(ma_list))

    mon_tuple = (3,4,"A")
    ma_list = [1,"plop", mon_tuple]

    ma_list.append(mon_tuple[2])
    
    #print(mon_tuple[1])
    #print(ma_list[2][1])

    mon_nouveau_tuple = tuple(ma_list)
    print(ma_list)

    print(list(enumerate(ma_list)))
    
    ma_var1, ma_var2 = ("v1", "v2")

    for index, element in enumerate(ma_list):
        print("index ", index, ":", element)

    print(list(range(10)))

    for i in range(10, 0, -1):
        print("i: %s" % i)

    ma_list = []
    num = 0
    while num < 10:
        ma_list.append(num)
        num += 1

    print(ma_list)

    lst_10 = list(range(10))

    lst_comp = [el**2 for el in lst_10]
    
    print(lst_comp)

    print("-"*79)
    print(lst_10[-1])
    print(lst_10[-2])

    print(lst_10[2:4])
    print(lst_10[:3])
    print(lst_10[2:])

    lst = [1,2,3]
    lst2 = list(lst)
    lst3 = lst
    lst[0] = 4
    
    print(lst[0] == lst2[0])
    print(lst[0] == lst3[0])

    t = (1, 2, 3, 4)
    un, deux, trois, quatre = t

    print(un, deux, trois)

    def test(arg1, arg2, arg3, arg4):
        print(arg1, arg2, arg3, arg4)

    test(t[0], t[1], t[2], t[3])
    test(*t)

    
    fun = lambda arg: arg*2
